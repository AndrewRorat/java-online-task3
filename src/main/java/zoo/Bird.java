package zoo;

public class Bird extends Animal{
    private boolean isFlying;
    private double maxFlightAltitude;
    private String kindOfBird;

    Bird(String foodRequesting, boolean isSleep, String name, boolean isWild, String habitat, boolean isFlying, double maxFlightAltitude, String kindOfBird) {
        super(foodRequesting, isSleep, name, isWild, habitat);
        this.isFlying = isFlying;
        this.maxFlightAltitude = maxFlightAltitude;
        this.kindOfBird = kindOfBird;
    }

    public String getKindOfBird() {
        return kindOfBird;
    }

    public void setKindOfBird(String kindOfBird) {
        this.kindOfBird = kindOfBird;
    }

    public boolean isFlying() {
        return isFlying;
    }

    public void setFlying(boolean flying) {
        isFlying = flying;
    }

    public double getMaxFlightAltitude() {
        return maxFlightAltitude;
    }

    public void setMaxFlightAltitude(float maxFlightAltitude) {
        this.maxFlightAltitude = maxFlightAltitude;
    }

    public String toString(){
        String out =    super.toString() +
                        "Is flying? " + isFlying() + "\n" +
                        "Maximum flight altitude is: " + getMaxFlightAltitude() + "km\n" +
                        "Kind of bird: " + getKindOfBird() + "\n";
        return out;
    }
}
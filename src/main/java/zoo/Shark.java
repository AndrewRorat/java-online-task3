package zoo;

public class Shark extends Fish {
    private boolean hasSharpTeeth;

    Shark(String foodRequesting, boolean isSleep, String name, boolean isWild,
                        boolean isHunter, String habitat, boolean hasSharpTeeth) {
        super(foodRequesting, isSleep, name, isWild, isHunter, habitat);
        this.hasSharpTeeth = hasSharpTeeth;
    }

    public boolean isHasSharpTeeth() {
        return hasSharpTeeth;
    }

    public void setHasSharpTeeth(boolean hasSharpTeeth) {
        this.hasSharpTeeth = hasSharpTeeth;
    }
    public String toString(){
        String out =    super.toString() +
                        "Has sharp teeth: " + isHasSharpTeeth() + "\n";
        return out;
    }
}

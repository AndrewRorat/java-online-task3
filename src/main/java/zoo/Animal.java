package zoo;

public class Animal {
    private String name;
    private boolean isSleep;
    private String foodRequesting;
    private boolean isWild;
    private String habitat;

    Animal(String foodRequesting, boolean isSleep, String name, boolean isWild, String habitat){
        this.foodRequesting = foodRequesting;
        this.isSleep = isSleep;
        this.name = name;
        this.isWild = isWild;
        this.habitat = habitat;
    }

    public String getHabitat() {
        return habitat;
    }

    public void setHabitat(String habitat) {
        this.habitat = habitat;
    }

    public String getName(String name) {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isSleep() {
        return isSleep;
    }

    public void setSleep(boolean sleep) {
        isSleep = sleep;
    }

    public String getFoodRequesting() {
        return foodRequesting;
    }

    public void setFoodRequesting(String foodRequesting) {
        this.foodRequesting = foodRequesting;
    }

    public boolean isWild() {
        return isWild;
    }

    public void setWild(boolean wild) {
        isWild = wild;
    }

    public String toString(){
        String out =    "Name: " + name + "\n" +
                        "FoodRequesting: " + getFoodRequesting() + "\n" +
                        "Wild? " + isWild() + "\n" +
                        "Is sleep? " + isSleep() + "\n" +
                        "Habitat: " + getHabitat() + "\n";
        return out;
    }
}
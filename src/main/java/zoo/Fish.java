package zoo;

public class Fish extends Animal{
    private boolean isHunter;

    Fish(String foodRequesting, boolean isSleep, String name, boolean isWild, boolean isHunter, String habitat){
        super(foodRequesting, isSleep, name, isWild, habitat);
        this.isHunter = isHunter;
    }

    public boolean isHunter() {
        return isHunter;
    }

    public void setHunter(boolean hunter) {
        isHunter = hunter;
    }

    @Override
    public String toString(){
        String out =    super.toString() +
                        "Is Hunter? " + isHunter()+ "\n";
        return out;
    }
}
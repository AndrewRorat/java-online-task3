package zoo;

public class Main {
    public static void main(String[] args) {

        Shark Sharky = new Shark("Meat", true, "Sharky", true,
                true, "Ocean and sea", true);

        Bird PostDove = new Bird("Anything", true, "Gul'-Gul'", true,
                "Distibuted throught the globe", true, 3.0, "Post dove");

        Bird WhiteHeadEagle = new Bird("Meat", true, "Hunter", true,
                "Distibuted throught the globe", true, 0.7, "White head eagle");

        System.out.println(Sharky);
        System.out.println(PostDove);
        System.out.println(WhiteHeadEagle);
    }
}
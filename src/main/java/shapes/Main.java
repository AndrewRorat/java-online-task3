package shapes;

public class Main {

    public static void main(String[] args) {
        Rectangle BlueRectangle = new Rectangle("Rectangle", "Blue", 4);
        Rectangle RedRectangle = new Rectangle("Rectangle", "Red", 4);
        Rectangle YellowRectangle = new Rectangle("Rectangle", "Yellow", 4);

        Circle BlueCircle = new Circle("Circle", "Blue", false);
        Circle RedCircle = new Circle("Circle", "red", false);
        Circle GreenCircle = new Circle("Circle", "Green", false);

        Triangle GreyTriangle = new Triangle("Triangle", "Grey", 3);
        Triangle OrangeTriangle = new Triangle("Triangle", "Orange", 3);
        Triangle PurpleTriangle = new Triangle("Triangle", "Purple", 3);


        System.out.printf("Types of rectangles are:\n\n" + BlueRectangle + "\n" + RedRectangle + "\n" + YellowRectangle + "\n\n");

        System.out.printf("Types of circles are:\n\n" + BlueCircle + "\n" + RedCircle + "\n" + GreenCircle + "\n\n");

        System.out.printf("Types of triangles are:\n\n" + GreyTriangle + "\n" + OrangeTriangle + "\n" + PurpleTriangle);
    }
}
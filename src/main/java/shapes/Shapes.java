package shapes;

public class Shapes {
    private String kindOfShape;
    private String color;

    public Shapes(String kindOfShape, String color) {
        this.kindOfShape = kindOfShape;
        this.color = color;
    }

    public String getKindOfShape() {
        return kindOfShape;
    }

    public void setKindOfShape(String kindOfShape) {
        this.kindOfShape = kindOfShape;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String toString(){
        String out =    "Kind of shape: " + getKindOfShape() + "\n" +
                        "Color is: " + getColor() + "\n";
        return out;
    }
}

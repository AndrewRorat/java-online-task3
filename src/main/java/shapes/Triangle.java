package shapes;

public class Triangle extends Shapes {
    private int angles = 3;
    public Triangle(String kindOfShape, String color,int angles) {
        super(kindOfShape, color);
        this.angles = angles;
    }

    public int getAngles() {
        return angles;
    }

    public void setAngles(int angles) {
        this.angles = angles;
    }
    public String toString(){
        String out =    super.toString() +
                "Number of angles are: " + getAngles() + "\n";
        return out;
    }
}

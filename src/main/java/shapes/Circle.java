package shapes;

public class Circle extends Shapes {
    private boolean hasAngles = false;
    public Circle(String kindOfShape, String color, boolean hasAngles) {
        super(kindOfShape, color);
        this.hasAngles = hasAngles;
    }

    public boolean isHasAngles() {
        return hasAngles;
    }

    public void setHasAngles(boolean hasAngles) {
        this.hasAngles = hasAngles;
    }
    public String toString(){
        String out =    super.toString() +
                        "Where there are angles? " + isHasAngles()+ "\n";
        return out;
    }
}
